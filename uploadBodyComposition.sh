#!/bin/bash

# Author: Leroy Zwakman

# This is a script that modifies the format from Fitdays a little bit, and push it to the runalyze API.

# This was an example taken from https://github.com/Runalyze/personal-api

csvfile=fitdays.csv

# Check if CSV file present
if [ ! -e ${csvfile} ]; then
    echo ${csvfile} not found
    exit 1
fi

# Keep track what we've logged to runanalyze, supports only one entry per date
if [ ! -e .logged ]; then
    touch .logged
fi

# read token
if [ -e .runalyze.token ]; then
	token=`cat .runalyze.token`
else
	if [ -z $1 ]; then
		echo "usage $0 <token>"
        echo "or setup a token"
		exit 1
	else
		token=$1
	fi
fi

# read the csv file into vars
while IFS="," read -r datetime weight bodyfat waterweight musclemass bonemass ; do

  # 15:35 22/01/2022 --> 2022-01-22
  _date=$(echo  ${datetime} | grep -Po '\d{1,2}/\d{1,2}/\d{4}' | tr "/" "-")
  _date=${_date:6:4}-${_date:3:2}-${_date:0:2}
  # 15:35
  _time=${datetime:1:5}

  # Get only decimals 10.2 from text, strip the rest
  _weight=$(echo ${weight} | grep -Po '[0-9\.]+')
  _bodyfat=$(echo ${bodyfat} | grep -Po '[0-9\.]+')
  _musclemass=$(echo ${musclemass} | grep -Po '[0-9\.]+')
  _waterweight=$(echo ${waterweight} | grep -Po '[0-9\.]+')

  # bonemass is in kg, transform to percentage
  _bonemass=$(echo ${bonemass} | grep -Po '[0-9\.]+')
  _bonemass=$(python -c "print(round(${_bonemass}*100/${_weight}, 1))" )

  if ! $(grep -Fxq "${_date}" .logged) ; then
    OUTPUT=$(curl -s -X POST "https://runalyze.com/api/v1/metrics/bodyComposition" \
      --header 'token: '${token}  \
      -d "{ \
          \"date_time\":\"${_date}T${_time}Z\", \
          \"weight\":${_weight}, \
          \"fat_percentage\": ${_bodyfat}, \
          \"water_percentage\": ${_waterweight}, \
          \"muscle_percentage\": ${_musclemass}, \
          \"bone_percentage\": ${_bonemass}
          }"
        )
    #TODO: implentation of error handling
    echo RC : $?
    echo $OUTPUT

    # keep track
    echo ${_date} >> .logged
    # don't hammer the API
    sleep 10
  else
     echo "${_date} has already been sent to Runalyze"
  fi
done < <(cut -d, -f1,2,4,9,11,12 ${csvfile} | tail -n +2 ) # skip header, and only take some colums from csv file

