# FitDays CSV Export to Runalyze

This is a very cheap scale you can fetch in the Action (in the Netherlands)
It's also known as Silvergear https://silvergear.eu/en/health/bluetooth-smart-scale-black/

This tells you how to convert the csv file to actual csv, and how to send it to the api.
The date is logged in a .logged file for duplication purposes. If something went wrong, delete this file


## CSV Export
This is not really a csvfile, but some kind of excel application: `CDFV2 Microsoft Excel`

### Instructions:
In the main screen, follow the steps:
- Chart (nl: Overzicht)
- History Records (nl: Gebuikersgegevens)
- select a date
- click on the + top right
- Choose Export (nl: Exporteren)
- Press this Month (for example)
- press the exit sign between the buttons on the bottom
- Save the file somewhere

In Linux, migrate the file to a csv file: (needed pacman packages are gnumeric psiconv
run the command:
```
ssconvert FitDays-export.csv fitdays.csv
```

Now the file should be in csv format, and usable for parsing and sending to api of runalyze.
I keep track of the latest date

### CSV File format
The file looks in dutch something like
```
Datum,Gewicht,BMI,Lichaamsvet,"Onderhuids vet",Hartslag,"Hart index","Visceraal vet",Watergewicht,Skeletspier,Spiermassa,Botmassa,Eiwit,BMR,Lichaamsleeftijd
"15:35 22/01/2022",60.5kg,19.5,13.6%,9.4%,"- -","- -",5.9,57.8%,59.4%,49.07kg,2.7kg,24.1%,1437.0kcal,34.0
"09:03 23/01/2022",60.4kg,19.5,13.5%,9.4%,"- -","- -",5.9,57.8%,59.5%,48.98kg,2.7kg,24.1%,1435.0kcal,34.0
"08:07 24/01/2022",60.1kg,19.4,13.3%,9.2%,"- -","- -",5.8,57.9%,59.6%,48.86kg,2.7kg,24.2%,1430.0kcal,34.0
"08:35 25/01/2022",60.1kg,19.4,13.3%,9.2%,"- -","- -",5.8,58.0%,59.6%,48.86kg,2.7kg,24.2%,1431.0kcal,34.0
```

### Setup API Key:
You will need an API key from runalyze, this can be fetch in your profile, create it with a local file:
```bash
echo -n '<token' > .runalyze.token` to prevent newlines
```

### Execution
Before running, make sure the csv file is CSV!, see above for instructions
Make sure the var in the file is having the right filename, e.g. fitdays.csv
```bash
bash uploadBodyComposition.sh
```

### Troubleshooting
- Make sure the file is converted to actual csv
- Make sure the name is correct before running the script
- Some magic is done for date time format, in dutch it should have been 24-01-2022 15:30:10 instead off 15:30 24/01/2022
  Since this is not something i can enforce, i change it, perhaps you have to do this for your language
